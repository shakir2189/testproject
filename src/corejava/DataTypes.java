package corejava;

public class DataTypes {

    public static void main(String[] args) {

        byte number1 = -128;
        byte number2 = 127;

        short number3 = -32_768;

        int number5 = -2_147_483_648;

        Float number8 = 1.222f;

        long number7 = 3535353545222l;

        double number9 = 1.33d;

        boolean x = true;

        char s = 's';
        char y = "|u00ae";
        System.out.println(y);

        System.out.println("|u00ae");


    }

}
